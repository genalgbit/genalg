import pyglet
from pyglet.gl import *
from math import *
import random as random
import Settings as Settings
import Gene as Gene
import Atom as Atom
import Energy_calculator as Energy_calculator
import Cluster as Cluster
import Mating_Mutating as mate

class Simulation():

    xmin = 0
    xmax = Settings.Settings.report_value(Settings.x_size)
    ymin = 0
    ymax = Settings.Settings.report_value(Settings.y_size)

    aradius = Settings.Settings.report_value(Settings.sigmaAA)
    bradius = Settings.Settings.report_value(Settings.sigmaBB)

    def __init__(self):
        self.population = [[[8.32359942, 15.87874273,2], [9.23991977, 15.22972034,2], [8.22017198, 14.75977149,1]],[[8.32359942, 15.87874273,2], [9.23991977, 15.22972034,2], [8.22017198, 14.75977149,1]]]

    def get_lowest(self):
        # Index of lowest chromosome is 1
        #return self.population[0]

        xsome = []
        for i in range(3):
            xsome.append([random.random()*100, random.random()*100, random.randint(1,2)])

        return xsome





def makeCircle(numPoints, radius, xcenter, ycenter):
    vertices = []
    for i in range(numPoints):
        angle = radians(float(i)/numPoints * 360.0)
        x = (radius)*cos(angle) + xcenter
        y = (radius)*sin(angle) + ycenter
        vertices += [x,y]  # adds a list to the list vertices --> makes a list of lists
    circle = pyglet.graphics.vertex_list((numPoints), ('v2f', vertices))
    return circle

class Window(pyglet.window.Window):
    def __init__(self):
        super(Window, self).__init__()

        self.simulation = Simulation()
        self.best_chromosome = None #self.simulation.get_lowest()     # gives the lowest energy chromosome using the get_lowest func
        self.xscale = 0
        self.yscale = 0

    def draw_circle(self):

        dict_of_clusters = {}
        list_of_clusters = []  # initialises list of clusters that will get updated through the GA

        while len(list_of_clusters) < Settings.Settings.report_value(Settings.C):  # creating C clusters
            atom_dict = {}
            atom_list = []

            while len(atom_list) < Settings.Settings.report_value(Settings.N):  # each cluster has N atoms
                gene_dict = {}
                gene_list = []

                # randomly assigns position coordinates to create x/ycoord genes
                x_coord = Settings.Settings.report_value(Settings.x_size) * (random.random())
                y_coord = Settings.Settings.report_value(Settings.y_size) * (random.random())
                # equal probability of A/B type assignment
                # A==1, B==2
                t = random.random()
                if t < 0.5:
                    atom_label = 1
                else:
                    atom_label = 2
                val_arr = [x_coord, y_coord, atom_label]
                for val in val_arr:
                    gene_dict['Gene' + str(Gene.Gene.index)] = Gene.Gene(val)
                    gene_list.append('Gene' + str(Gene.Gene.index))

                # print len(gene_list)

                # Atom-type object creation:
                atom_dict['Atom' + str(Atom.Atom.index)] = Atom.Atom(gene_list, gene_dict)
                atom_list.append('Atom' + str(Atom.Atom.index))

            # checks if the cluster is garbage; if not, creates cluster
            energy = Energy_calculator.energy_calculator(atom_list, atom_dict)
            if energy > 0.05:
                for atom in atom_list:
                    del atom_dict[atom]  # cleans up unneeded memory
                    # Atom.index -= N  # rewinds the Atom index - initialisation of large clusters could cause integer overrun
            else:
                # print atom_list
                dict_of_clusters['Cluster' + str(Cluster.Cluster.index)] = Cluster.Cluster(atom_list, atom_dict)
                list_of_clusters.append('Cluster' + str(Cluster.Cluster.index))

        # make a dictionary of energies of all clusters
        dict_of_cluster_energies = {}  # make that into a property of cluster object
        for cluster in list_of_clusters:
            dict_of_cluster_energies[cluster] = Cluster.Cluster.cluster_energy(dict_of_clusters[cluster])

        fail_count = 0
        steps = 0
        while min(dict_of_cluster_energies.values()) > -2.9999:
            # while fail_count < 10000:
            # choice of two random integers for list of clusters
            a = int(Settings.Settings.report_value(Settings.C) * random.random())
            b = int(Settings.Settings.report_value(Settings.C) * random.random())

            if a == b:  # try again if the same cluster is picked twice - parthenogenesis is useless
                continue

            # calls parent clusters and retrieves their atom lists/dicts ... better than retrieving them always
            parent1 = dict_of_clusters[list_of_clusters[a]]
            # p1_alist = Cluster.Cluster.atoms_list_in_cluster(dict_of_clusters[parent1])
            # p1_adict = Cluster.Cluster.atoms_dict_in_cluster(dict_of_clusters[parent1])

            parent2 = dict_of_clusters[list_of_clusters[b]]
            # p2_alist = Cluster.Cluster.atoms_list_in_cluster(dict_of_clusters[parent2])
            # p2_adict = Cluster.Cluster.atoms_dict_in_cluster(dict_of_clusters[parent2])

            # print p1_adict, p1_alist, p2_adict, p2_alist
            steps += 1

            child1 = mate.mating_procedure(Cluster.Cluster.output_cluster(parent1),
                                           Cluster.Cluster.output_cluster(parent2))

            thelist = []
            thedict = {}
            for mylist in child1:
                thegene_dict = {}
                thegene_list = []
                for val in mylist:
                    thegene_dict['Gene' + str(Gene.Gene.index)] = Gene.Gene(val)
                    thegene_list.append('Gene' + str(Gene.Gene.index))

                thedict['Atom' + str(Atom.Atom.index)] = Atom.Atom(thegene_list, thegene_dict)
                thelist.append('Atom' + str(Atom.Atom.index))

            maxenergy = Energy_calculator.energy_calculator(thelist, thedict)
            if maxenergy < max(dict_of_cluster_energies.values()):
                thekey = max(dict_of_cluster_energies, key=dict_of_cluster_energies.get)
                del dict_of_clusters[thekey]
                del dict_of_cluster_energies[thekey]
                list_of_clusters.remove(thekey)
                dict_of_clusters['Cluster' + str(Cluster.Cluster.index)] = Cluster.Cluster(thelist, thedict)
                list_of_clusters.append('Cluster' + str(Cluster.Cluster.index))
                dict_of_cluster_energies['Cluster' + str(Cluster.Cluster.index)] = Cluster.Cluster.cluster_energy(
                    dict_of_clusters['Cluster' + str(Cluster.Cluster.index)])
                fail_count = 0
            else:
                for atom in thelist:
                    del thedict[atom]  # cleans up unneeded memory
                # Atom.index -= Settings.record_value(N)
                fail_count += 1

            child2 = mate.mating_procedure(Cluster.Cluster.output_cluster(parent2),
                                           Cluster.Cluster.output_cluster(parent1))

            thelist = []
            thedict = {}
            for mylist in child2:
                thegene_dict = {}
                thegene_list = []
                for val in mylist:
                    thegene_dict['Gene' + str(Gene.Gene.index)] = Gene.Gene(val)
                    thegene_list.append('Gene' + str(Gene.Gene.index))

                thedict['Atom' + str(Atom.Atom.index)] = Atom.Atom(thegene_list, thegene_dict)
                thelist.append('Atom' + str(Atom.Atom.index))

            energy = Energy_calculator.energy_calculator(thelist, thedict)
            if energy < max(dict_of_cluster_energies.values()):
                thekey = max(dict_of_cluster_energies, key=dict_of_cluster_energies.get)
                del dict_of_clusters[thekey]
                del dict_of_cluster_energies[thekey]
                list_of_clusters.remove(thekey)
                dict_of_clusters['Cluster' + str(Cluster.Cluster.index)] = Cluster.Cluster(thelist, thedict)
                list_of_clusters.append('Cluster' + str(Cluster.Cluster.index))
                dict_of_cluster_energies['Cluster' + str(Cluster.Cluster.index)] = Cluster.Cluster.cluster_energy(
                    dict_of_clusters['Cluster' + str(Cluster.Cluster.index)])
                fail_count = 0
            else:
                for atom in thelist:
                    del thedict[atom]  # cleans up unneeded memory
                # Atom.index -= Settings.record_value(N)
                fail_count += 1

            print 'f', steps, fail_count, max(dict_of_cluster_energies.values()), min(dict_of_cluster_energies.values())
            # sigmaAA = 1
            # sigmaBB = 1.3
            # sigmaAB = ((sigmaAA + sigmaBB) / 2)

            # Send population to simulation self.simulation.population = self.current_population
            # get best chromosome.
            # Receive population for safekeeping. self.current_population = self.simulation.population



            sum_of_x = 0
            sum_of_y = 0
            # self.best_chromosome = self.simulation.get_lowest()
            self.best_chromosome = Cluster.Cluster.output_cluster(dict_of_clusters[min(dict_of_cluster_energies, key=dict_of_cluster_energies.get)])

            for element in self.best_chromosome:  # get the coords for the lowest energy chromosome
                x = element[0]
                y = element[1]
                sum_of_x += x
                sum_of_y += y

            xavg = (sum_of_x) / len(self.best_chromosome)
            yavg = (sum_of_y) / len(self.best_chromosome)

            centre_of_x = (self.width) / 2.0
            centre_of_y = (self.height) / 2.0

            dx = centre_of_x - xavg     # dx & dy addition to x & y centre the cluster on screen
            dy = centre_of_y - yavg

            for element in self.best_chromosome:  # get the coords for the lowest energy chromosome
                x = element[0]
                y = element[1]
                label = element[2]

                if label == 1:  # Atom is A type
                    glColor3f(1, 0, 0)

                    r = self.simulation.aradius * sqrt(self.xscale * self.yscale)

                    circ = makeCircle(2000, r, x + dx , y + dy)  # populate the drawList with apart@global minimum

                    circ.draw(GL_LINE_LOOP)  # draw the circle

                elif label == 2:  # Atom is B type
                    glColor3f(0, 1, 0)

                    r = self.simulation.bradius * sqrt(self.xscale * self.yscale)

                    #  circ = makeCircle(2000, 27, x + dx, y + dy) # populate the drawList with touching@global minimum
                    circ = makeCircle(2000, r, x + dx, y + dy)  # populate the drawList with apart@global minimum

                    circ.draw(GL_LINE_LOOP)  # draw the circle

    def on_draw(self):

        glClear(pyglet.gl.GL_COLOR_BUFFER_BIT)

        self.width = 800
        self.height = 500

        self.xscale = self.width / float(self.simulation.xmax)
        self.yscale = self.height / float(self.simulation.ymax)

        self.draw_circle()



    def update(self,dt):
        glClear(pyglet.gl.GL_COLOR_BUFFER_BIT)

        self.draw_circle()

if __name__ == '__main__':
    window = Window()                                 # initialize a window class
    pyglet.clock.schedule_interval(window.update, 1.0/2.0)  # tell pyglet how often it should execute on_draw() & update()
    pyglet.app.run()                                      # run pyglet