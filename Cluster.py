import Atom
import Energy_calculator as Energy_calculator


class Cluster:
    """Cluster class contains cluster-type/chromosome objects comprising atom-type objects."""
    index = 0

    def __init__(self, list_of_atoms, dict_of_atoms):
        """Creates cluster from a list of atoms"""
        self._list_of_atoms = list_of_atoms
        self._dict_of_atoms = dict_of_atoms
        Cluster.index += 1

    def atoms_list_in_cluster(self):
        """Returns list of atoms in the cluster"""
        return self._list_of_atoms

    def atoms_dict_in_cluster(self):
        """Returns dictionary of atoms in the cluster"""
        return self._dict_of_atoms

    def cluster_energy(self):
        """Calculates the energy of a cluster by looping through all pairs of atoms in the cluster"""
        l = Cluster.atoms_list_in_cluster(self)
        d = Cluster.atoms_dict_in_cluster(self)

        energy = Energy_calculator.energy_calculator(l, d)
        return energy

    def output_cluster(self):
        """Returns cluster as list of lists"""
        list_of_lists = []
        l = Cluster.atoms_list_in_cluster(self)
        d = Cluster.atoms_dict_in_cluster(self)

        for atom in l:
            list_of_lists.append(Atom.Atom.report_pos_and_label(d[atom]))

        return list_of_lists
