import pyglet
from pyglet.gl import *
from math import *
from random import *
import test

def makeCircle(numPoints, radius, xcenter, ycenter):
    vertices = []
    for i in range(numPoints):
        angle = radians(float(i)/numPoints * 360.0)
        x = (radius)*cos(angle) + xcenter
        y = (radius)*sin(angle) + ycenter
        vertices += [x,y]  # adds a list to the list vertices --> makes a list of lists
    circle = pyglet.graphics.vertex_list((numPoints), ('v2f', vertices))
    return circle

class Window(pyglet.window.Window):
    def __init__(self):
        super(Window, self).__init__()

        self.simulation = test.Simulation()
        self.best_chromosome = None #self.simulation.get_lowest()     # gives the lowest energy chromosome using the get_lowest func
        self.xscale = 0
        self.yscale = 0

    def draw_circle(self):
        # sigmaAA = 1
        # sigmaBB = 1.3
        # sigmaAB = ((sigmaAA + sigmaBB) / 2)

        # Send population to simulation self.simulation.population = self.current_population
        # get best chromosome.
        # Receive population for safekeeping. self.current_population = self.simulation.population

        sum_of_x = 0
        sum_of_y = 0
        self.best_chromosome = self.simulation.get_lowest()

        for element in self.best_chromosome:  # get the coords for the lowest energy chromosome
            x = element[0]
            y = element[1]
            sum_of_x += x
            sum_of_y += y

        xavg = (sum_of_x) / len(self.best_chromosome)
        yavg = (sum_of_y) / len(self.best_chromosome)

        centre_of_x = (self.width) / 2.0
        centre_of_y = (self.height) / 2.0

        dx = centre_of_x - xavg     # dx & dy addition to x & y centre the cluster on screen
        dy = centre_of_y - yavg

        for element in self.best_chromosome:  # get the coords for the lowest energy chromosome
            x = element[0]
            y = element[1]
            label = element[2]

            if label == 1:  # Atom is A type
                glColor3f(1, 0, 0)

                r = self.simulation.aradius * sqrt(self.xscale * self.yscale)

                circ = makeCircle(2000, r, x + dx , y + dy)  # populate the drawList with apart@global minimum

                circ.draw(GL_LINE_LOOP)  # draw the circle

            elif label == 2:  # Atom is B type
                glColor3f(0, 1, 0)

                r = self.simulation.bradius * sqrt(self.xscale * self.yscale)

                #  circ = makeCircle(2000, 27, x + dx, y + dy) # populate the drawList with touching@global minimum
                circ = makeCircle(2000, r, x + dx, y + dy)  # populate the drawList with apart@global minimum

                circ.draw(GL_LINE_LOOP)  # draw the circle

    def on_draw(self):

        glClear(pyglet.gl.GL_COLOR_BUFFER_BIT)

        self.width = 800
        self.height = 500

        self.xscale = self.width / float(self.simulation.xmax)
        self.yscale = self.height / float(self.simulation.ymax)

        self.draw_circle()



    def update(self,dt):
        glClear(pyglet.gl.GL_COLOR_BUFFER_BIT)

        self.draw_circle()

if __name__ == '__main__':
    window = Window()                                 # initialize a window class
    pyglet.clock.schedule_interval(window.update, 1.0/2.0)  # tell pyglet how often it should execute on_draw() & update()
    pyglet.app.run()                                      # run pyglet