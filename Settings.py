import numpy as np


class Settings:
    """Class of variable settings for simulations"""

    def __init__(self, value):
        self._value = value

    def report_value(self):
        """Returns the value"""
        return self._value


N = Settings(3)  # total number of atoms in the cluster
C = Settings(400)  # starting number of clusters

# lists sigmas; sigmaBB needs fixing to allow list of sigmaBBs
sigmaAA = Settings(1)  #
sigmaBB = Settings(1.3)  #

sigmaAB = Settings((Settings.report_value(sigmaAA) + Settings.report_value(sigmaBB)) / 2)

# the size of the 2D space:
x_size = Settings(np.sqrt(2 * Settings.report_value(N))*Settings.report_value(sigmaBB))
y_size = Settings(np.sqrt(2 * Settings.report_value(N))*Settings.report_value(sigmaBB))
