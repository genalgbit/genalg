# import numpy as np
import random as random
import Settings as Settings
import Gene as Gene
import Atom as Atom
import Energy_calculator as Energy_calculator
import Cluster as Cluster
import Mating as mate

dict_of_clusters = {}
list_of_clusters = []  # initialises list of clusters that will get updated through the GA

while len(list_of_clusters) < Settings.Settings.report_value(Settings.C):  # creating C clusters
    atom_dict = {}
    atom_list = []

    while len(atom_list) < Settings.Settings.report_value(Settings.N):  # each cluster has N atoms
        gene_dict = {}
        gene_list = []

        # randomly assigns position coordinates to create x/ycoord genes
        x_coord = Settings.Settings.report_value(Settings.x_size) * (random.random())
        y_coord = Settings.Settings.report_value(Settings.y_size) * (random.random())
        # equal probability of A/B type assignment
        # A==1, B==2
        t = random.random()
        if t < 0.5:
            atom_label = 1
        else:
            atom_label = 2
        val_arr = [x_coord, y_coord, atom_label]
        for val in val_arr:
            gene_dict['Gene' + str(Gene.Gene.index)] = Gene.Gene(val)
            gene_list.append('Gene' + str(Gene.Gene.index))

        # print len(gene_list)

        # Atom-type object creation:
        atom_dict['Atom' + str(Atom.Atom.index)] = Atom.Atom(gene_list, gene_dict)
        atom_list.append('Atom' + str(Atom.Atom.index))

    # checks if the cluster is garbage; if not, creates cluster
    energy = Energy_calculator.energy_calculator(atom_list, atom_dict)
    if energy > 0.05:
        for atom in atom_list:
            del atom_dict[atom]  # cleans up unneeded memory
        # Atom.index -= N  # rewinds the Atom index - initialisation of large clusters could cause integer overrun
    else:
        # print atom_list
        dict_of_clusters['Cluster' + str(Cluster.Cluster.index)] = Cluster.Cluster(atom_list, atom_dict)
        list_of_clusters.append('Cluster' + str(Cluster.Cluster.index))


#  make a dictionary of energies of all clusters
dict_of_cluster_energies = {}
for cluster in list_of_clusters:
    dict_of_cluster_energies[cluster] = Cluster.Cluster.cluster_energy(dict_of_clusters[cluster])


fail_count = 0
steps = 0
while min(dict_of_cluster_energies.values()) > -2.9999:
# while fail_count < 10000:
    # choice of two random integers for list of clusters
    a = int(Settings.Settings.report_value(Settings.C) * random.random())
    b = int(Settings.Settings.report_value(Settings.C) * random.random())

    if a == b:  # try again if the same cluster is picked twice - parthenogenesis is useless
        continue

    # calls parent clusters and retrieves their atom lists/dicts ... better than retrieving them always
    parent1 = dict_of_clusters[list_of_clusters[a]]
    # p1_alist = Cluster.Cluster.atoms_list_in_cluster(dict_of_clusters[parent1])
    # p1_adict = Cluster.Cluster.atoms_dict_in_cluster(dict_of_clusters[parent1])

    parent2 = dict_of_clusters[list_of_clusters[b]]
    # p2_alist = Cluster.Cluster.atoms_list_in_cluster(dict_of_clusters[parent2])
    # p2_adict = Cluster.Cluster.atoms_dict_in_cluster(dict_of_clusters[parent2])

    # print p1_adict, p1_alist, p2_adict, p2_alist
    steps += 1
    child1 = mate.mating_procedure(Cluster.Cluster.output_cluster(parent1), Cluster.Cluster.output_cluster(parent2))

    thelist = []
    thedict = {}
    for mylist in child1:
        thegene_dict = {}
        thegene_list = []
        for val in mylist:
            thegene_dict['Gene' + str(Gene.Gene.index)] = Gene.Gene(val)
            thegene_list.append('Gene' + str(Gene.Gene.index))

        thedict['Atom' + str(Atom.Atom.index)] = Atom.Atom(thegene_list, thegene_dict)
        thelist.append('Atom' + str(Atom.Atom.index))

    energy = Energy_calculator.energy_calculator(thelist, thedict)
    if energy < max(dict_of_cluster_energies.values()):
        thekey = max(dict_of_cluster_energies, key=dict_of_cluster_energies.get)
        del dict_of_clusters[thekey]
        del dict_of_cluster_energies[thekey]
        list_of_clusters.remove(thekey)
        dict_of_clusters['Cluster' + str(Cluster.Cluster.index)] = Cluster.Cluster(thelist, thedict)
        list_of_clusters.append('Cluster' + str(Cluster.Cluster.index))
        dict_of_cluster_energies['Cluster' + str(Cluster.Cluster.index)] = Cluster.Cluster.cluster_energy(dict_of_clusters['Cluster' + str(Cluster.Cluster.index)])
        fail_count = 0
    else:
        for atom in thelist:
            del thedict[atom]  # cleans up unneeded memory
        # Atom.index -= Settings.record_value(N)
        fail_count += 1

    child2 = mate.mating_procedure(Cluster.Cluster.output_cluster(parent2), Cluster.Cluster.output_cluster(parent1))

    thelist = []
    thedict = {}
    for mylist in child2:
        thegene_dict = {}
        thegene_list = []
        for val in mylist:
            thegene_dict['Gene' + str(Gene.Gene.index)] = Gene.Gene(val)
            thegene_list.append('Gene' + str(Gene.Gene.index))

        thedict['Atom' + str(Atom.Atom.index)] = Atom.Atom(thegene_list, thegene_dict)
        thelist.append('Atom' + str(Atom.Atom.index))

    energy = Energy_calculator.energy_calculator(thelist, thedict)
    if energy < max(dict_of_cluster_energies.values()):
        thekey = max(dict_of_cluster_energies, key=dict_of_cluster_energies.get)
        del dict_of_clusters[thekey]
        del dict_of_cluster_energies[thekey]
        list_of_clusters.remove(thekey)
        dict_of_clusters['Cluster' + str(Cluster.Cluster.index)] = Cluster.Cluster(thelist, thedict)
        list_of_clusters.append('Cluster' + str(Cluster.Cluster.index))
        dict_of_cluster_energies['Cluster' + str(Cluster.Cluster.index)] = Cluster.Cluster.cluster_energy(dict_of_clusters['Cluster' + str(Cluster.Cluster.index)])
        fail_count = 0
    else:
        for atom in thelist:
            del thedict[atom]  # cleans up unneeded memory
        # Atom.index -= Settings.record_value(N)
        fail_count += 1

    print 'f', steps, fail_count, max(dict_of_cluster_energies.values()), min(dict_of_cluster_energies.values())
