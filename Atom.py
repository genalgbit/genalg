import numpy as np
import Gene as Gene


class Atom:
    """Atom class contains atom-type objects that form clusters."""
    index = 0

    def __init__(self, genelist, genedict):
        """Creates an atom with position coordinates and a type"""
        self._list_of_genes = genelist
        self._dict_of_genes = genedict
        Atom.index += 1

    def genes_list_in_atom(self):
        """Returns list of genes in the atom"""
        return self._list_of_genes

    def genes_dict_in_atom(self):
        """Returns dict of genes in the atom"""
        return self._dict_of_genes

    def report_xcoord(self):
        """Returns the xcoord of atom."""
        return Gene.Gene.report_value(self._dict_of_genes[self._list_of_genes[0]])

    def report_ycoord(self):
        """Returns the ycoord of atom."""
        return Gene.Gene.report_value(self._dict_of_genes[self._list_of_genes[1]])

    def report_label(self):
        """Returns the label of atom."""
        return Gene.Gene.report_value(self._dict_of_genes[self._list_of_genes[2]])

    def report_pos(self):
        """Returns the coordinates of the atom"""
        pos_vect = np.array([Atom.report_xcoord(self), Atom.report_ycoord(self)])
        return pos_vect

    def report_pos_and_label(self):
        """Returns all three genes of the atom as the_atom=[xcoord, ycoord, label]"""
        the_atom = [Atom.report_xcoord(self), Atom.report_ycoord(self), Atom.report_label(self)]
        return the_atom
