import numpy as np


class Distance(object):

    def __init__(self):
        # self.x = x
        # self.y = y
        pass

    def centerofmass(self):
        """Here, 'masses' are the positions of all points in the cluster.
        The third number in the array is the weighing. Do not change this
        unless not all atoms are equal."""
        masses = np.array([[1, 2, 1],
                           [1, 2, 1],
                           [1, 2, 1],
                           [1, 2, 1],
                           [1, 2, 1]])

        nonZeroMasses = masses[np.nonzero(masses[:, 2])]

        CM = np.average(nonZeroMasses[:, :2], axis=0, weights=nonZeroMasses[:, 2])

        return CM #this is a vector for the center of positions in the cluster



dist = Distance()

    # The two vectors below are arbitrary mutli-atom vectors X and Y (in the paper).
    # X and Y are simply subsets of the cluster and have positions associated with them.
    # Obviously, the positions given here are testing positions.

vector_x = np.array([[2, 9],
                     [5, 8]])
vector_y = np.array([[3, 6],
                     [6, 3]])

def ordx():

    ord_x = vector_x - dist.centerofmass()

    return ord_x

def ordy():

    ord_y = vector_y - dist.centerofmass()

    return ord_y

# ord_x and ord_y should be ordered vectors of X and Y respectively. They are calculated
# by minusing the center of mass from the X array and the Y array.

print dist.centerofmass()
ord_x = ordx()
ord_y = ordy()

print ord_x
sum_of_cubes = 0 #sum over all N atoms starts here where len(ord_x) = len(ord_y) = N
for i in range(len(ord_x)):
    ord_x_row = ord_x[i, :]
    ord_y_row = ord_y[i, :]
    term = ord_x_row - ord_y_row
    dot = np.dot(term, term)
    cube = dot**1.5
    sum_of_cubes += cube

print sum_of_cubes

c_root = (sum_of_cubes)**(1.0/3.0)

print c_root

# c_root is the distance_ord, d. In the paper, they quote d^3.

