class Gene:
    """Gene class contains x and y coords (float-types) and identities (int-types)"""
    index = 0

    def __init__(self, value):
        self._value = value
        Gene.index += 1

    def report_value(self):
        """reports the value of the gene"""
        return self._value
