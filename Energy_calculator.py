import numpy as np
import Atom as Atom
import Settings as Settings


def energy_calculator(atomlist, atomdict):
    """Calculates energy of a set of atoms"""
    l = atomlist
    d = atomdict

    sigma_dict = {(1, 1): Settings.Settings.report_value(Settings.sigmaAA), (1, 2): Settings.Settings.report_value(Settings.sigmaAB), (2, 1): Settings.Settings.report_value(Settings.sigmaAB), (2, 2): Settings.Settings.report_value(Settings.sigmaBB)}
    energy = 0  # initialises energy
    for i in xrange(Settings.Settings.report_value(Settings.N) - 1):
        for j in xrange(i + 1, Settings.Settings.report_value(Settings.N)):
            type_i = Atom.Atom.report_label(d[l[i]])
            type_j = Atom.Atom.report_label(d[l[j]])
            atom_pair = (type_i, type_j)
            sigma = sigma_dict[atom_pair]  # picks appropriate sigma for the calculation

            pos_i = Atom.Atom.report_pos(d[l[i]])
            pos_j = Atom.Atom.report_pos(d[l[j]])
            d_pos = pos_j - pos_i
            r_ij = np.sqrt(np.dot(d_pos, d_pos))  # distance between centres of atoms

            frac6 = (sigma / r_ij) ** 6  # makes calculation faster

            energy += 4 * ((frac6 ** 2) - frac6)
    return energy
