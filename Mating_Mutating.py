import numpy as np
import random
import Settings as Settings

# ================ Functions needed by the main mating function ======================

# function that creates the LP1 and LP2 lists
def LP_list(cut_point,coord_parent,size):
    distance_CP = {}  # Contains distances squared of every atom from the cut point and the index is the atom number

    for i in range(size):
        distance = (cut_point[0] - coord_parent[i][0]) ** 2 + (cut_point[1] - coord_parent[i][1]) ** 2
        distance_CP.update({i: distance})

    # this contains the index of the atoms in order of increasing distance from CP
    LP = sorted(distance_CP, key=distance_CP.__getitem__)

    return LP

# function that checks whether the son has the right amount of label 1 atoms


def right_label(count1_D1,count2_D1, count1_P1, count2_P1, current_label):
    if count1_D1<count1_P1 and count2_D1<count2_P1:
        accept = True
    elif count1_D1 == count1_P1:
        if current_label == 2:
            accept = True
        else:
            accept = False
    elif count2_D1 == count2_P1:
        if current_label == 1:
            accept = True
        else:
            accept = False
    return accept


# this function checks that the atom of coord_parent2 is not overlapping with any atom in D1

def overlap_child(coord_atom, D1, size):
    space = True        # tracks if there enough space between particles
    for i in range(size):
        if D1[i]== 0:
            continue
        distance = np.sqrt((D1[i][0] - coord_atom[0])**2 + (D1[i][1] - coord_atom[1])**2)
        if distance < 0.5:
            space = False
            break
    return space



# ==============================================
#
#           Main mating function
#
# ==============================================

# Takes as an input the coordinates of parent 1 and parent 2, and returns the coordinates of child 1

def mating_procedure(coord_parent1, coord_parent2):

    # =================== choosing the cut of point from the parent 1 ======================

    size = len(coord_parent1)

    cut_point_index = random.randint(0,size-1)   # This takes a random number between 0 and the number of atoms in a cluster
    cut_point = coord_parent1[cut_point_index]   # Coordinates of cut point atom


    # =================== creating list LP1  and LP2 ==========================

    # these lists contain the indexes of atoms with increasing distance from cut point

    LP1 = LP_list(cut_point,coord_parent1,size)
    LP2 = LP_list(cut_point,coord_parent2,size)


    # ================== Generating S ===============================

    S = random.randint(0,size-2)        # Generating a random number that corresponds to the range 1,N-1 in 0 indexing

    # ==================== Making D1 ==============================

    # putting the first S (because S starts from zero) atoms from P1 in the child list
    # at the same time checking the number of atoms of label 1 are in D1

    D1 = [0] * size
    count1_P1 = 0  # counter for the number of atoms of label 1 in P1
    count1_D1 = 0  # counter for the number of atoms of label 1 in D1
    count2_D1 = 0

    for i in range(0,S+1):
        D1[i] = coord_parent1[LP1[i]]      # Takes the coordinates atom of the atom in P1 with index LP1[i]
        if D1[i][2] == 1:  # Counts how many atoms 1 there are in D1
            count1_D1 += 1
        elif D1[i][2] == 2:  # Counts how many atoms 2 there are in D1
            count2_D1 += 1

    for i in range(size):                   # Counts how many atoms 1 there are in P1
        if coord_parent1[i][2] == 1:
            count1_P1 += 1

    count2_P1 = size - count1_P1  # The number of atoms of label 2 in the parent 1

    # completing D1 with the rest of atoms from LP2, making sure that the number of atoms of label 1 is the same as in P1

    for i in range(S+1,size):       # loop over D1
        for j in range(size):       # loop over LP2
            # checks that the new atom from parent 2 doesnt overlap with those already in D1
            condition1 = overlap_child(coord_parent2[LP2[j]],D1,size)
            # This checks that the number of atoms of a certain label are the same as in P1
            condition2 = right_label(count1_D1, count2_D1, count1_P1, count2_P1, coord_parent2[LP2[j]][2])
            # if both conditions are satisfied, the atom from parent 2 goes into D1
            if condition1 and condition2:
                D1[i] = coord_parent2[LP2[j]]
                # if the new atom is of type 1, the counter is incremented
                if coord_parent2[LP2[j]][2] == 1:
                    count1_D1 += 1
                    break
                elif coord_parent2[LP2[j]][2] == 2:
                    count2_D1 += 1
                    break

    # checking that D1 is full, if not an atom of random type is added at a random position that doesn't overlap with
    # the other atoms

    for i in range(size):
        while D1[i] == 0:
            random_atom = [random.uniform(-5.0,5.0), random.uniform(-5.0,5.0), random.randint(1,2)]
            condition1 = overlap_child(random_atom,D1,i)
            if condition1:
                D1[i] = random_atom

    # Introducing mutations in the child
    # position mutation
    sigma_mut = 0.05 * Settings.Settings.report_value(Settings.sigmaBB) * 2 ** (1.0 / 6.0) * np.sqrt(size)
    for i in range(size):
        D1[i][0] = D1[i][0] + np.random.normal(0.0, sigma_mut)
        D1[i][1] = D1[i][1] + np.random.normal(0.0, sigma_mut)

    # label mutation
    for i in range(size):
        rate_switch_mutation = 0.025    # rate from paper
        if np.random.uniform(0.0,1.0) <= rate_switch_mutation:
            if D1[i][2] == 1:
                D1[i][2] == 2
            else:
                D1[i][2] == 1

    return D1
















